#[doc = "<s>Vulkan Manual Page</s> · Constant"]
#[doc(alias = "VK_NV_FILL_RECTANGLE_SPEC_VERSION")]
pub const NV_FILL_RECTANGLE_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
#[doc(alias = "VK_NV_FILL_RECTANGLE_EXTENSION_NAME")]
pub const NV_FILL_RECTANGLE_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_NV_fill_rectangle");
