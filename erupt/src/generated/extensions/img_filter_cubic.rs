#[doc = "<s>Vulkan Manual Page</s> · Constant"]
#[doc(alias = "VK_IMG_FILTER_CUBIC_SPEC_VERSION")]
pub const IMG_FILTER_CUBIC_SPEC_VERSION: u32 = 1;
#[doc = "<s>Vulkan Manual Page</s> · Constant"]
#[doc(alias = "VK_IMG_FILTER_CUBIC_EXTENSION_NAME")]
pub const IMG_FILTER_CUBIC_EXTENSION_NAME: *const std::os::raw::c_char = crate::cstr!("VK_IMG_filter_cubic");
